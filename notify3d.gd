@tool
extends Node3D

signal stuffhappened

func _ready() -> void:
	set_notify_local_transform(true)

func _notification(what: int) -> void:
	if what == NOTIFICATION_LOCAL_TRANSFORM_CHANGED:
		stuffhappened.emit()
