@tool
extends Node2D

## Transform Lesson
##
## PARENT.transform * CHILD.position --> position in the node-space
## that the PARENT node exists in.
##
## There are two examples in one here.
## First: The player node exists *in* the field node.
## The ball (and boy) exist in the player node.
## The player node exists in the field node.
## To go from the ball node to the field (player's parent) node, you do this:
##   `player.transform * ball.position`
## This will be a Vector3 that exists in the field's space (player's parent).
##
## Second: The flag node exists in the field node.
## To find where it exists in the PLAYER node, you have to reverse the
## process of the math:
##   `flag.position * player.transform`
## This is a Vector3 that exists in the PLAYER's space!
## Move the flag around and check it against the other numbers.

func do():
	%ball/lab.text = "Ball:\nPos in player: %3.1v\nPos within field: %3.1v" % \
		[%ball.position, %player.transform * %ball.position]

	%boy/lab.text = "Boy:\nPos in player: %3.1v\nPos within field: %3.1v" % \
		[%boy.position, %player.transform * %boy.position]


func _on_stuffhappened() -> void:
	do()


func _on_confirm_stuffhappened() -> void:
	%confirm/lab.text = "Confirm\nPos in field: %3.1v\nPos within player: %3.1v\nPos in player/boy: %3.1v" % \
		[
			%confirm.position,
			%confirm.position * %player.transform,
			%confirm.position * %player.transform *  %boy.transform
		]

func _on_flag_stuffhappened() -> void:
	%flag/lab.text = "Flag — How to go DOWN into child nodes\nPos in field: %3.1v\nPos within player: %3.1v" % [%flag.position, %flag.position * %player.transform]
