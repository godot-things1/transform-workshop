@tool
extends Node2D

## Click the "Go" boolean to run the code. Move the ball around and watch the
## differences when you alter the true/false ifs in the code.

@export var go:bool:
	set(b):
		go = false
		demo()

func demo():
	var t : Transform2D = transform
	print("transform:", t)
	var ti := t.affine_inverse()
	print("affine_inverse:", ti)
	t = ti * t
	print("affine_inverse * transform:", t)

	print()
	print("Original position:", position)

	var np := Vector2(100,200)

	## Play with the true/false ifs here to see what happens:

	if true:
		# this is 100,200 relative to my
		# transform, i.e. further
		position = transform * np
		print("position set relative:", position)

	if false:
		# this is 100,200 absolute
		position = np
		print(" position set absolute:", position)

	if false:
		# unfalse this to see how the numbers change.
		# The first ti (above) hasn't got the latest transform,
		# So, recalc the affine_inverse.
		ti = transform.affine_inverse()

	print("position moved:", position)

	position = ti * position
	print("inverse * position:", position)
