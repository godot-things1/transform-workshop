@tool
extends Node3D

## Basis Demo
##
## Basis * Basis.inverse() -> A Basis of the difference

## @Devilbox (Mastodon)
## @dbat a Basis is a rotation matrix, you have the current rotation basis and the
##  target you want to end up with.
##
##  Multiplying something by a basis applies that rotation to it,
##  BUT inverse does the opposite.
##
##  This is often called transforming into the "space" of that basis.
##  Multiplying a basis by the inverse of another basically gives you the
##  difference in rotation between the two, which you can manipulate
##  (e.g. limit the rotation) and then multiply by the current basis
##  to get a new target basis.

@onready var a: MeshInstance3D = $A
@onready var b: MeshInstance3D = $B
@onready var c: MeshInstance3D = $C

func foo():
	print("***")
	var Ab := a.basis
	print("a basis:", Ab)
	var Bb := b.basis
	print("b basis:", Bb)
	print("b.inverse():", Bb.inverse())

	# I think this is saying : take B's rotation away from A's
	var difference := Ab * Bb.inverse()
	print(" diff:", difference)

	c.basis = difference


func _on_stuffhappened() -> void:
	foo()
